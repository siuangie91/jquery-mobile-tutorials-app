# Tutorial Videos App #

Project from Rob Percival's [**Complete Web Developer Course**](https://www.udemy.com/complete-web-developer-course/) on **Udemy**. A tutorial app reminiscent of Udemy, Lynda.com, etc. Built using jQuery Mobile.

## Landing Page ##
![Screen Shot 2015-12-05 at 19.23.36.png](https://bitbucket.org/repo/apMjA9/images/4037310530-Screen%20Shot%202015-12-05%20at%2019.23.36.png)

## Navigation ##
![Screen Shot 2015-12-05 at 19.23.49.png](https://bitbucket.org/repo/apMjA9/images/1798707607-Screen%20Shot%202015-12-05%20at%2019.23.49.png)

## Search ##
![Screen Shot 2015-12-05 at 19.24.01.png](https://bitbucket.org/repo/apMjA9/images/2455195774-Screen%20Shot%202015-12-05%20at%2019.24.01.png)

## Video Page ##
![Screen Shot 2015-12-05 at 19.24.10.png](https://bitbucket.org/repo/apMjA9/images/3514045909-Screen%20Shot%202015-12-05%20at%2019.24.10.png)